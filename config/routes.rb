Rails.application.routes.draw do
  resources :lists


  resources :tasks
  get 'welcome/show' => 'welcome#show'
  root 'welcome#show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
