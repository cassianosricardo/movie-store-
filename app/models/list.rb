class List < ApplicationRecord
	has_many :tasks, dependent: :destroy 

	def to_s
		self.name
	end
end
